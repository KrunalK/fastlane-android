## Fastlane Setup ##

Step1 : Install fastlane

    sudo gem install fastlane –verbose

Step2 : move into the folder where your APK is located and run (using cd 'path')

    fastlane init

Step3 : goto 

    (Project Directory)/fastlane/Fastfile 
and overwrite Fastfile.

Step4 : open Fastfile and
1. setup Fabric API & Secret key
2. set tester & client email id.

Step5 : Run this command

    fastlane beta
or

    fastlane development


Ref. https://docs.fastlane.tools/

Ref. https://docs.fabric.io/unity/beta/distribute-beta-builds.html

Ref. https://docs.fastlane.tools/actions/#gradle

## Module level build.gradle ##


```
#!python

android {
    compileSdkVersion 25
    buildToolsVersion "24.0.2"

    defaultConfig {
        applicationId "com.fastlanedemo"
        minSdkVersion 15
        targetSdkVersion 25
        versionCode rootProject.VERSION_BUILD.toInteger()
        versionName "1.0." + rootProject.VERSION_BUILD.toInteger()
    }

    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
        }
    }

    // Hook to check if the release/debug task is among the tasks to be executed.
    //Let's make use of it
    gradle.taskGraph.whenReady {taskGraph ->
        if (taskGraph.hasTask(assembleDebug)) {   //when run debug task
            autoIncrementBuildNumber()
        } else if (taskGraph.hasTask(assembleRelease)) {  //when run release task
            autoIncrementBuildNumber()
        }
    }
}
```

```
#!python
ext {
    def versionPropsFile = rootProject.file('gradle.properties')

    /*Wrapping inside a method avoids auto incrementing on every gradle task run. Now it runs only when we build apk*/
    autoIncrementBuildNumber = {
        if (versionPropsFile.canRead()) {
            def versionBuild
            def Properties versionProps = new Properties()
            versionProps.load(new FileInputStream(versionPropsFile))
            if (rootProject.IS_UPDATE_VERSION.toBoolean()) {
                versionBuild = versionProps['VERSION_BUILD'].toInteger() + 1
                versionProps['VERSION_BUILD'] = versionBuild.toString()
            }
            versionProps.store(versionPropsFile.newWriter(), null)
        } else {
            throw new GradleException("Could not read version.properties!")
        }
    }
}
```

## gradle.properties ##


```
#!python

VERSION_BUILD = 1
IS_UPDATE_VERSION = false
```